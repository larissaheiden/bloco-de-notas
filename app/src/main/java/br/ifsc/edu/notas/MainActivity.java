package br.ifsc.edu.notas;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.putString("nota", editText.getText().toString());
                editor.commit();

            }
        });

        //configuração do SharedPreferences

        sharedPreferences= getSharedPreferences("notas", MODE_PRIVATE);
        editor=sharedPreferences.edit();

    }

    @Override
    protected void onStart() {
        super.onStart();

        editText.setText(sharedPreferences.getString("notas", ""));
    }
}
